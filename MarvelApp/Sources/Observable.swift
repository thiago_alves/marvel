//
//  Observable.swift
//  MarvelApp
//
//  Created by talves on 17/05/19.
//  Copyright © 2019 com.talves.MarvelApp. All rights reserved.
//

class Observable<T> {
    fileprivate var _value: T?
    var didChange: ((T) -> Void)?
    var value: T {
        set {
            _value = newValue
            didChange?(_value!)
        }
        get {
            return _value!
        }
    }

    init(_ value: T) {
        self.value = value
    }

    deinit {
        _value = nil
        didChange = nil
    }
}
