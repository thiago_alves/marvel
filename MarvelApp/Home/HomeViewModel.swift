//
//  HomeViewModel.swift
//  MarvelApp
//
//  Created by talves on 17/05/19.
//  Copyright © 2019 com.talves.MarvelApp. All rights reserved.
//

import UIKit

class HomeViewModel {

    // MARK: - Properties
    var characterList: [Characteres]
    weak var delegate: HomeCoordinator?

    // MARK: - Observables
    var characterObservable: Observable<RequestStates<[Characteres]>> = Observable(RequestStates.loading)

    // MARK: - Control
    var isLoadingFromServer = false
    var reachedListEnd = false

    //MARK: - Initialization
    init(with characteres: [Characteres]) {
        self.characterList = characteres
    }

    func getcharacteres(offset: Int) {

        isLoadingFromServer = true
        self.characterObservable.value = .loading
        
        API().homeServices.characterList(offset: offset, success: { characteres in

            self.isLoadingFromServer = false

            if characteres.isEmpty {
                self.reachedListEnd = true
            }

            self.characterList.append(contentsOf: characteres)
            self.characterObservable.value = .load(data: characteres)
        }, failure: { [weak self] error in

            self?.isLoadingFromServer = false

            self?.characterObservable.value = .errored(error: error)
        })
    }

    func showDetail(character: Characteres) {
        delegate?.showDetail(character: character)
    }
}
