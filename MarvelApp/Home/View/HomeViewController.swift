//
//  HomeViewController.swift
//  MarvelApp
//
//  Created by Thiago Felipe Alves on 16/05/19.
//  Copyright © 2019 com.talves.MarvelApp. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!

    // MARK: - Properties
    var viewModel: HomeViewModel?

    var indexOfPageToRequest = 0

    // MARK: - Initialization
    init(viewModel: HomeViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        registerCells()
        addObservers()
        self.title = "MARVEL"

        self.viewModel?.getcharacteres(offset: indexOfPageToRequest)
    }

    // MARK: - Private Methods
    private func registerCells() {
        collectionView.register(CharactersCollectionViewCell.self)
    }

    private func addObservers() {

        self.viewModel?.characterObservable.didChange = { [weak self] status in

            guard let self = self else { return }

            switch status {
            case .load:
                DispatchQueue.main.async {
                    self.view.hideHUD()
                    self.collectionView.reloadData()
                }
            case .loading:
                DispatchQueue.main.async {
                    self.view.showHUD()
                }
            case .errored(error: let error):
                DispatchQueue.main.async {
                    self.view.hideHUD()
                    self.view.showError(error: error, uiview: self)
                }
            }
        }
    }
}

//MARK: - UICollectionViewDataSource, UICollectionViewDelegate
extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel?.characterList.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let characteres = viewModel?.characterList else {
            return UICollectionViewCell()
        }

        let cell = collectionView.dequeueReusableCell(of: CharactersCollectionViewCell.self, for: indexPath) as! CharactersCollectionViewCell
        cell.setup(with: characteres[indexPath.row])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        guard let viewModel = self.viewModel else {
            return
        }

        viewModel.showDetail(character: viewModel.characterList[indexPath.row])
    }
}

//MARK: - UICollectionViewDelegateFlowLayout
extension HomeViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = (collectionView.frame.size.width - 32) / 2
        return CGSize(width: width, height: width * 1.5)

    }
}

// MARK: - UIScrollViewDelegate
extension HomeViewController: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        guard let viewModel = self.viewModel else {
            return
        }

        if viewModel.isLoadingFromServer || viewModel.reachedListEnd {
            return
        }

        // calculates where the user is in the y-axis
        let offsetY = scrollView.contentOffset.y
        if offsetY <= 0 {
            return
        }

        let contentHeight = scrollView.contentSize.height
        if (offsetY + 700.0) > (contentHeight - scrollView.frame.size.height) {
            // increments the number of the page to request
            indexOfPageToRequest += 1

            // call your API for more data
            let skip = indexOfPageToRequest * 20

            // call your API for more data
            //let skip = indexOfPageToRequest * kPaginationTake
            viewModel.getcharacteres(offset: skip)
        }
    }
}
