//
//  CharactersCollectionViewCell.swift
//  MarvelApp
//
//  Created by talves on 17/05/19.
//  Copyright © 2019 com.talves.MarvelApp. All rights reserved.
//

import UIKit
import Kingfisher

class CharactersCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgCharacters: UIImageView!
    @IBOutlet weak var lblName: UILabel!

    func setup(with character: Characteres) {

        imgCharacters.image = nil
        lblName.text = character.name

        let url = URL(string: "\(character.thumbnail.path).\(character.thumbnail.photoExtension)")
        imgCharacters.kf.setImage(with: url)
    }
}
