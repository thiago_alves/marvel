 //
//  HomeCoordinator.swift
//  MarvelApp
//
//  Created by Thiago Felipe Alves on 16/05/19.
//  Copyright © 2019 com.talves.MarvelApp. All rights reserved.
//

import UIKit

 protocol HomeCoordinatorDelegate: AnyObject {
    func showDetail(character: Characteres)
 }

class HomeCoordinator: NSObject {

    var window: UIWindow

    var viewController: HomeViewController?
    var viewModel: HomeViewModel?
    var navigation: UINavigationController?

    var detailCoordinator: DetailCoordinator?

    required init(window: UIWindow) {
        self.window = window
    }

    func start() {

        viewModel = HomeViewModel(with: [])
        viewModel?.delegate = self
        viewController = HomeViewController(viewModel: viewModel!)

        navigation = UINavigationController(rootViewController: viewController!)
        navigation?.navigationBar.barTintColor = .red
        navigation?.navigationBar.tintColor = .white
        navigation?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigation?.navigationBar.isOpaque = false

        window.rootViewController = navigation
    }
}

 extension HomeCoordinator: HomeCoordinatorDelegate {

    func showDetail(character: Characteres) {
        detailCoordinator = DetailCoordinator(character: character)
        self.navigation?.pushViewController(detailCoordinator!.start(), animated: true)
    }
 }
