//
//  HomeAPI.swift
//  MarvelApp
//
//  Created by Thiago Felipe Alves on 16/05/19.
//  Copyright © 2019 com.talves.MarvelApp. All rights reserved.
//

import UIKit

class HomeAPI: Requester {

    func characterList(limit: Int = 20,
                       offset: Int,
                       success: @escaping CompletionWithSuccess<[Characteres]>,
                       failure: @escaping CompletionWithFailure) {

        let header = HeaderHandler().generate(header: .basic)
        let url = urlComposer(using: Endpoint.Home.characters,
                              complement: "?limit=\(limit)&offset=\(offset)&\(MarvelKey.getHash())")
        let requester = requestComposer(using: url as! (url: URL, method: String), headers: header)

        dataTask(using: requester) { data, error in

            if error == nil, let data = data {

                let model = self.JSONDecode(to: StatusModel.self, from: data)
                guard let resultModel = model else {
                    failure(self.getParseError(data: data))
                    return
                }
                success(resultModel.data.characters)
            }
            //
        }
    }
}
