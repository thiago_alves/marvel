//
//  MarvelKey.swift
//  MarvelApp
//
//  Created by Thiago Felipe Alves on 16/05/19.
//  Copyright © 2019 com.talves.MarvelApp. All rights reserved.
//

import UIKit
import CommonCrypto

extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}

class MarvelKey {

    private static let publicApi = "fc57002bb2fbe41286059f37f5af59ad"
    private static let privateApi = "16298e7f6b6fff6cbe92a99d7449ac2a004a3cbe"

    static func getHash() -> String {

        let ticks = Date().ticks

        let hash = "\(ticks)\(privateApi)\(publicApi)"
        return "ts=\(ticks)&apikey=\(publicApi)&hash=\(md5(hash))"
    }

    static private func md5(_ string: String) -> String {

        let context = UnsafeMutablePointer<CC_MD5_CTX>.allocate(capacity: 1)
        var digest = Array<UInt8>(repeating:0, count:Int(CC_MD5_DIGEST_LENGTH))
        CC_MD5_Init(context)
        CC_MD5_Update(context, string, CC_LONG(string.lengthOfBytes(using: String.Encoding.utf8)))
        CC_MD5_Final(&digest, context)
        context.deallocate(capacity: 1)
        var hexString = ""
        for byte in digest {
            hexString += String(format:"%02x", byte)
        }

        return hexString
    }
}
