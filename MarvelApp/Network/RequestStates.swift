//
//  RequestStates.swift
//  MarvelApp
//
//  Created by Thiago Felipe Alves on 16/05/19.
//  Copyright © 2019 com.talves.MarvelApp. All rights reserved.
//

import Foundation

enum RequestStates<T> {
    case loading
    case errored(error: NSError)
    case load(data: T)
}
