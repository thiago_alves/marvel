//
//  API.swift
//  MarvelApp
//
//  Created by Thiago Felipe Alves on 16/05/19.
//  Copyright © 2019 com.talves.MarvelApp. All rights reserved.
//

import Foundation

//---------------- API -----------------------
class API {
    enum Environment: String {

        case production = "https://gateway.marvel.com"

        //Just to be more readable
        func getValue() -> String {
            return self.rawValue
        }
    }

    let environment: Environment

    init() {
        environment = Environment.production
    }

    lazy var homeServices = HomeAPI()
}
