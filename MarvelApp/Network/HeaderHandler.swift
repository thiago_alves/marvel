//
//  HeaderHandler.swift
//  MarvelApp
//
//  Created by Thiago Felipe Alves on 16/05/19.
//  Copyright © 2019 com.talves.MarvelApp. All rights reserved.
//

import UIKit

struct HeaderHandler {

    enum HeaderType {
        case basic
    }

    func generate(header: HeaderType) -> [String: String] {
        switch header {
        case .basic:
            return self.getBasicHeader()
        }
    }

    private func getBasicHeader() -> [String: String] {
        return ["Content-Type": "application/json"]
    }

}

