//
//  Endpoint.swift
//  MarvelApp
//
//  Created by Thiago Felipe Alves on 16/05/19.
//  Copyright © 2019 com.talves.MarvelApp. All rights reserved.
//

import Foundation

// //---------------- ENDPOINTS -----------------------
public class Endpoint {
    /// A tuple that recieves an URI and the http request method
    typealias EndpointType = (uri: String, method: String)

    /// Contains the http method String simplified for
    struct HTTPMethod {
        static let get = "GET"
        static let post = "POST"
        static let update = "PUT"
        static let delete = "DELETE"
        static let head = "HEAD"
    }

    // Endpoints list
    struct Home {
        static let characters: EndpointType = (uri: "/v1/public/characters", method: HTTPMethod.get)
    }
}
