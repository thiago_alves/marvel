//
//  DetailCoordinator.swift
//  MarvelApp
//
//  Created by Thiago Felipe Alves on 19/05/19.
//  Copyright © 2019 com.talves.MarvelApp. All rights reserved.
//

import UIKit

class DetailCoordinator: NSObject {

    // MARK: - Properties
    var navigation: UINavigationController?

    var view: DetailViewController?
    var viewModel: DetailViewModel?

    init(character: Characteres) {

        viewModel = DetailViewModel(with: character)
        view = DetailViewController(viewModel: viewModel!)
    }

    func start() -> UIViewController {
        return view!
    }
}
