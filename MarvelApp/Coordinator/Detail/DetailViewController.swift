//
//  DetailViewController.swift
//  MarvelApp
//
//  Created by Thiago Felipe Alves on 19/05/19.
//  Copyright © 2019 com.talves.MarvelApp. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var imgCharactere: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!

    // MARK: - Properties
    var viewModel: DetailViewModel?

    // MARK: - Initialization
    init(viewModel: DetailViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    // MARK: - Private Methods
    private func setupUI() {

        guard let character = self.viewModel?.character else {
            return
        }

        self.title = character.name
        
        let url = URL(string: "\(character.thumbnail.path).\(character.thumbnail.photoExtension)")
        self.imgCharactere.kf.setImage(with: url)

        self.lblName.text = character.name
        self.lblDescription.text = character.characterDescription
    }
}
