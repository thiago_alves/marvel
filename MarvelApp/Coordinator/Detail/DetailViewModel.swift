//
//  DetailViewModel.swift
//  MarvelApp
//
//  Created by Thiago Felipe Alves on 19/05/19.
//  Copyright © 2019 com.talves.MarvelApp. All rights reserved.
//

import UIKit

class DetailViewModel {

    // MARK: - Properties
    var character: Characteres

    //MARK: - Initialization
    init(with character: Characteres) {
        self.character = character
    }
}
